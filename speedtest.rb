HOST = '10.0.10.1'
FILE_LIST = ['test1KB','test2KB','test4KB','test8KB','test16KB',
			'test32KB','test64KB','test128KB','test256KB','test512KB',
			'test1MB','test2MB','test4MB','test8MB','test16MB',
			'test32MB','test64MB','test128MB','test256MB','test512MB',
			'test1GB','test2GB','test4GB']

REPORTED_PATH = '/home/anres/reported/'
ROUND = 1
TASK = ['http','tsunami','ftp']

load 'test_http.rb'
load 'test_tsunami.rb'
load 'test_ftp.rb'


TASK.each do |task_name|
    if task_name == 'http' then
    	test_http HOST, FILE_LIST
    elsif task_name == 'tsunami'
        test_tsunami HOST, FILE_LIST
	elsif task_name == 'ftp'
		test_ftp HOST, FILE_LIST
    end

end

puts "END."
