def test_tsunami host,file_list
	file_list.each do |file_name|
		f = File.open(REPORTED_PATH + 'tsunami-' + file_name , 'a')
		(1..ROUND).each do |i|
			puts 'TSUNAMI - ' + file_name + ' (' + i.to_s + '/' + ROUND.to_s + ') >> '
			time_start = Time.now			
			tmp = `tsunami set output screen connect #{host} get #{file_name} < exit_cmd > /dev/null`
			time_finish = Time.now
			time_usage = time_finish - time_start
			f.puts(time_usage)
			puts "================== #{time_usage} OK  ================================"
			`rm #{file_name}`
			sleep 1.0
		end 	
	end
end
