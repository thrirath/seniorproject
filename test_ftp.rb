def test_ftp host, file_list
	file_list.each do |file_name|
		f = File.open(REPORTED_PATH + 'ftp-' + file_name , 'a')
		(1..ROUND).each do |i|
			print 'FTP - ' + file_name + ' (' + i.to_s + '/' + ROUND.to_s + ') >> '
			time_start = Time.now
			tmp = `wget --delete-afte ftp://#{host}/#{file_name} -o /dev/null`
			time_finish = Time.now
			time_usage = time_finish - time_start
			f.puts(time_usage)
			puts time_usage.to_s + ' OK.'
			sleep 1.0
		end
		f.close
	end
end
